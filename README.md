
# Installation

- `nvm use`
- `yarn`
- `node install.js` (initializes Sqlite DB models)

You can also use the `example-db.sqlite` DB, this is the same data as described in the Task 2 example info in PDF. Just rename the file to `db.sqlite`

# API

Default hostname is `http://localhost:3000`

If inputting data, request body needs to be valid JSON (except for `/script-parser`, that needs to plain text)

To run: `yarn run:api`

**Endpoints**

- GET `/company/:id` - Fetch company 
- PUT `/company` - Create new company
- DELETE `/company/:id` - Delete company
- PATCH `/company/:id` - Update company
- GET `/company/:id/stations` - Fetches stations for company (and possible child companies)


- GET `/station/:id` - Fetch station
- PUT `/station` - Create new station
- DELETE `/station/:id` - Delete station
- PATCH `/station/:id` - Update station


- GET `/station-type/:id` - Fetch stationType
- PUT `/station-type` - Create new stationType
- DELETE `/station-type/:id` - Delete stationType
- PATCH `/station-type/:id` - Update stationType


- POST `/script-parser` - Parse & output script (Task 2)
