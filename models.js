const Sequelize = require('sequelize').Sequelize;
const DataTypes = require('sequelize').DataTypes;

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'db.sqlite'
});

const Company = sequelize.define('Company', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: DataTypes.STRING,
});

const Station = sequelize.define('Station', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: DataTypes.STRING
});

const StationType = sequelize.define('StationType', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: DataTypes.STRING,
    maxPower: DataTypes.INTEGER
});

// Relations

Company.hasMany(Station);
Company.hasOne(Company, {
    foreignKey: 'parentCompany',
    constraints: false
}); // Child companies

Station.belongsTo(Company);
Station.belongsTo(StationType);

StationType.hasOne(Station);

module.exports = {
    sequelize,
    Company,
    Station,
    StationType,
}
