const db = require("./models");

const express = require('express');
const { Op } = require("sequelize");
const {StationType} = require("./models");

// COMPANY

const companyRouter = express.Router();

companyRouter.get("/:id", async (req, res) => {
    const company = await db.Company.findByPk(req.params.id);
    if(company) {
        res.json(company);
    } else {
        res.send('not found').status(404);
    }
});

companyRouter.delete("/:id", async (req, res) => {
    const company = await db.Company.findByPk(req.params.id);
    if(company) {
        await company.destroy();
        res.json({status: 'ok'});
    } else {
        res.send('not found').status(404);
    }
});

companyRouter.patch("/:id", async (req, res) => {
    const company = await db.Company.findByPk(req.params.id);
    if(company) {
        if(req.body.name !== undefined) {
            company.name = req.body.name;
        }
        if(req.body.parentCompany !== undefined) {
            company.parentCompany = req.body.parentCompany;
        }
        if(company.changed()) {
            company.save();
        }
        res.json({
            status: 'ok',
            company
        });
    } else {
        res.send('not found').status(404);
    }
});

companyRouter.put("/", async (req, res) => {
    // At least name is required
    if(req.body.name !== undefined) {
        try {
            const company = await db.Company.create({
                name: req.body.name,
                parentCompany: (req.body.parentCompany !== undefined) ? req.body.parentCompany : 0
            });
            res.json({
                status: 'ok',
                company
            });
        } catch (e) {
            res.json({ status: 'error' }).status(400);
        }
    } else {
        res.send('input error').status(400);
    }
});

// Endpoint that responds with all stations of the company and possible child companies
companyRouter.get("/:id/stations", async (req, res) => {

    const company = await db.Company.findByPk(req.params.id);

    if(company) {

        // Check if company has child companies
        const childCompanies = await db.Company.findAll({
           where: {
               parentCompany: company.id
           }
        });

        // Build array of company ids we are looking for
        var companyIds = [company.id];

        childCompanies.forEach(childCompany => {
           companyIds.push(childCompany.id);
        });

        // Get all stations for the company and possible child companies
        var stations = await db.Station.findAll({
            include: StationType,
            where: {
                CompanyId: {
                    [Op.in]: companyIds
                }
            }
        });

        res.json({
            status: 'ok',
            stations
        });

    } else {
        res.send('not found').status(404);
    }

});

// STATION

const stationRouter = express.Router();

stationRouter.get("/:id", async (req, res) => {
    const station = await db.Station.findByPk(req.params.id);
    if(station) {
        res.json(station);
    } else {
        res.send('not found').status(404);
    }
});

stationRouter.delete("/:id", async (req, res) => {
    const station = await db.Station.findByPk(req.params.id);
    if(station) {
        await station.destroy();
        res.json({status: 'ok'});
    } else {
        res.send('not found').status(404);
    }
});

stationRouter.patch("/:id", async (req, res) => {
    const station = await db.Station.findByPk(req.params.id);
    if(station) {
        if(req.body.name !== undefined) {
            station.name = req.body.name;
        }
        if(req.body.CompanyId !== undefined) {
            station.CompanyId = req.body.CompanyId;
        }
        if(req.body.StationTypeId !== undefined) {
            station.StationTypeId = req.body.StationTypeId;
        }
        if(station.changed()) {
            station.save();
        }
        res.json({
            status: 'ok',
            company: station
        });
    } else {
        res.send('not found').status(404);
    }
});

stationRouter.put("/", async (req, res) => {
    // At least name, CompanyId & StationTypeId are required
    if(req.body.name !== undefined && req.body.CompanyId !== undefined && req.body.StationTypeId !== undefined) {
        try {
            const station = await db.Station.create({
                name: req.body.name,
                CompanyId: req.body.CompanyId,
                StationTypeId: req.body.StationTypeId
            });
            res.json({
                status: 'ok',
                station
            });
        } catch (e) {
            res.json({ status: 'error' }).status(400);
        }
    } else {
        res.send('input error').status(400);
    }
});

// STATIONTYPE

const stationTypeRouter = express.Router();

stationTypeRouter.get("/:id", async (req, res) => {
    const stationType = await db.StationType.findByPk(req.params.id);
    if(stationType) {
        res.json(stationType);
    } else {
        res.send('not found').status(404);
    }
});

stationTypeRouter.delete("/:id", async (req, res) => {
    const stationType = await db.StationType.findByPk(req.params.id);
    if(stationType) {
        await stationType.destroy();
        res.json({status: 'ok'});
    } else {
        res.send('not found').status(404);
    }
});

stationTypeRouter.patch("/:id", async (req, res) => {
    const stationType = await db.StationType.findByPk(req.params.id);
    if(stationType) {
        if(req.body.name !== undefined) {
            stationType.name = req.body.name;
        }
        if(req.body.maxPower !== undefined) {
            stationType.maxPower = parseInt(req.body.maxPower);
        }
        if(stationType.changed()) {
            stationType.save();
        }
        res.json({
            status: 'ok',
            stationType
        });
    } else {
        res.send('not found').status(404);
    }
});

stationTypeRouter.put("/", async (req, res) => {
    // At least name is required
    if(req.body.name !== undefined) {
        try {
            const stationType = await db.StationType.create({
                name: req.body.name,
                maxPower: (req.body.maxPower !== undefined) ? parseInt(req.body.maxPower) : 0
            });
            res.json({
                status: 'ok',
                company: stationType
            });
        } catch (e) {
            res.json({ status: 'error' }).status(400);
        }
    } else {
        res.send('input error').status(400);
    }
});

// SCRIPTPARSER

const scriptParserRouter = express.Router();

scriptParserRouter.post("/", async (req, res) => {
   if(req.body) {

       var beginLine = false;
       var endLine = false;

       // Look for Begin and End markers
       const allLines = req.body.split("\n");
       allLines.forEach((line, index) => {
           if (line.toUpperCase().trim() === "BEGIN") {
               beginLine = index;
           } else if (line.toUpperCase().trim() === "END") {
               endLine = index;
           }
       });
       if(beginLine !== false && endLine !== false) {

           // Found begin and end markers, get lines between them
           const lines = allLines.slice(beginLine, endLine + 1);

           // Initialize values
           var steps = [];
           var chargingStatus = {};
           var lineParameter = "";
           const companies = await db.Company.findAll();
           const stations = await db.Station.findAll();
           const stationTypes = await db.StationType.findAll();
           // Dummy timestamp
           var timestamp = 1000;

           // Make companies keyed
           var companiesKeyed = {};
           companies.forEach(company => {
               companiesKeyed[company.id] = company;
           });

           // Make stations keyed
           var stationsKeyed = {};
           stations.forEach(station => {
              stationsKeyed[station.id] = station;
           });

           // Make station types keyed
           var stationTypesKeyed = {};
           stationTypes.forEach(stationType => {
               stationTypesKeyed[stationType.id] = stationType;
           });

           // Initialize charging statuses for stations
           Object.keys(stationTypesKeyed).forEach(key => {
               stationTypesKeyed[key].charging = false;
           });

           lines.forEach(line => {
                if(line.toUpperCase().trim().indexOf("START STATION") === 0) {
                    // 'Start station' line

                    lineParameter = line.toUpperCase().trim().replace("START STATION", "").trim();

                    if(lineParameter === "ALL" || parseInt(lineParameter)) {
                        // Has a valid parameter
                        if(lineParameter === "ALL") {
                            // Start all stations
                            Object.keys(stationsKeyed).forEach(key => {
                                stationsKeyed[key].charging = true;
                            });
                        } else {
                            // Start station based on id
                            stationsKeyed[parseInt(lineParameter)].charging = true;
                        }
                    }

                    chargingStatus = determineCurrentChargingStatus(companiesKeyed, stationsKeyed, stationTypesKeyed);
                    steps.push({
                        step: 'Start station ' + lineParameter.toLowerCase(),
                        timestamp: timestamp,
                        companies: chargingStatus.companies,
                        totalChargingStations: chargingStatus.totalChargingStations,
                        totalChargingPower: chargingStatus.totalChargingPower
                    });

                } else if(line.toUpperCase().trim().indexOf("STOP STATION") === 0) {
                    // 'Stop station' line

                    lineParameter = line.toUpperCase().trim().replace("STOP STATION", "").trim();

                    if(lineParameter === "ALL" || parseInt(lineParameter)) {
                        // Has a valid parameter
                        if(lineParameter === "ALL") {
                            // Stop all stations
                            Object.keys(stationsKeyed).forEach(key => {
                                stationsKeyed[key].charging = false;
                            });
                        } else {
                            // Stop station based on id
                            stationsKeyed[parseInt(lineParameter)].charging = false;
                        }
                    }

                    chargingStatus = determineCurrentChargingStatus(companiesKeyed, stationsKeyed, stationTypesKeyed);
                    steps.push({
                        step: 'Stop station ' + lineParameter.toLowerCase(),
                        timestamp: timestamp,
                        companies: chargingStatus.companies,
                        totalChargingStations: chargingStatus.totalChargingStations,
                        totalChargingPower: chargingStatus.totalChargingPower
                    });

                } else if(line.toUpperCase().trim() === "BEGIN") {
                    // 'Begin' line
                    chargingStatus = determineCurrentChargingStatus(companiesKeyed, stationsKeyed, stationTypesKeyed);
                    steps.push({
                        step: 'Begin',
                        timestamp: timestamp,
                        companies: chargingStatus.companies,
                        totalChargingStations: chargingStatus.totalChargingStations,
                        totalChargingPower: chargingStatus.totalChargingPower
                    });
                } else if(line.toUpperCase().trim() === "END") {
                    // 'End' line
                    chargingStatus = determineCurrentChargingStatus(companiesKeyed, stationsKeyed, stationTypesKeyed);
                    steps.push({
                        step: 'End',
                        timestamp: timestamp,
                        companies: chargingStatus.companies,
                        totalChargingStations: chargingStatus.totalChargingStations,
                        totalChargingPower: chargingStatus.totalChargingPower
                    });
                } else if(line.toUpperCase().trim().indexOf("WAIT") === 0) {
                    // 'Wait' line

                    lineParameter = line.toUpperCase().trim().replace("WAIT", "").trim();

                    if(parseInt(lineParameter)) {
                        // Has a valid parameter
                        timestamp += parseInt(lineParameter);
                    }
                }
           });

           res.json({
               data: steps
           });

       } else {
           res.send("line marker error").status(400);
       }

   } else {
       res.send("input error").status(400);
   }
});

function determineCurrentChargingStatus(companies, stations, stationTypes) {

    var totalChargingStations = [];
    var totalChargingPower = 0;

    var companyStatuses = {};

    Object.keys(stations).forEach(stationId => {
        if(stations[stationId].charging) {
            // Increment totals
            totalChargingStations.push(parseInt(stationId));
            totalChargingPower += stationTypes[stations[stationId].StationTypeId].maxPower;

            // Check company specific status
            var company = companies[stations[stationId].CompanyId];

            if(company.parentCompany === 0) {
                // This is a parent company's station
                companyStatuses = updateCompanyStatus(companyStatuses, company, stationId, stationTypes[stations[stationId].StationTypeId].maxPower);
            } else {
                // This is a child company's station
                // We need to update the values for both this station and its parent
                companyStatuses = updateCompanyStatus(companyStatuses, company, stationId, stationTypes[stations[stationId].StationTypeId].maxPower);

                // Load parent company
                company = companies[company.parentCompany];
                companyStatuses = updateCompanyStatus(companyStatuses, company, stationId, stationTypes[stations[stationId].StationTypeId].maxPower);
            }

        }
    });

    return {
        companies: Object.values(companyStatuses),
        totalChargingStations,
        totalChargingPower
    }
}

function updateCompanyStatus(companyStatuses, company, stationId, maxPower) {
    if(companyStatuses[company.id] === undefined) {
        companyStatuses[company.id] = {
            id: company.id,
            chargingStations: [],
            chargingPower: 0
        }
    }
    companyStatuses[company.id].chargingStations.push(parseInt(stationId));
    companyStatuses[company.id].chargingPower += maxPower;
    return companyStatuses;
}

module.exports = {
    companyRouter,
    stationRouter,
    stationTypeRouter,
    scriptParserRouter
};
