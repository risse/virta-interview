const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const apiPort = 3000;

const routes = require('./routes');

app.use(bodyParser.json());
app.use(bodyParser.text());

app.use("/company", routes.companyRouter);
app.use("/station", routes.stationRouter);
app.use("/station-type", routes.stationTypeRouter);
app.use("/script-parser", routes.scriptParserRouter);

app.listen(apiPort);
